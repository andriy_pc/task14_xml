

<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:x="planes">
    <xsl:template match="/">
        <html>
            <body>
                <title>Planes</title>
                <h1>Planes</h1>
                <table border="1px solid gray">
                    <tr>
                        <td>Model</td>
                        <td>Origin</td>
                        <td>Type</td>
                        <td>Sits</td>
                        <td>Length</td>
                        <td>Width</td>
                        <td>Height</td>
                        <td>Price</td>
                    </tr>
                    <xsl:for-each select="x:Planes/x:Plane">
                        <xsl:sort select="x:Chars" />
                        <tr>
                            <td><xsl:value-of select="x:Model"/></td>
                            <td><xsl:value-of select="x:Origin"/></td>
                            <xsl:for-each select="x:Chars">
                                <xsl:sort select="x:type" />
                                <td><xsl:value-of select="x:type"/></td>
                                <td><xsl:value-of select="x:sits"/></td>
                            </xsl:for-each>
                            <xsl:for-each select="x:Parameters">
                                <td><xsl:value-of select="x:length"/></td>
                                <td><xsl:value-of select="x:width"/></td>
                                <td><xsl:value-of select="x:height"/></td>
                            </xsl:for-each>
                            <td><xsl:value-of select="x:Price"/></td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>