package units;

public class Chars {
    private String type;
    private int rockets;
    private int sits;
    private boolean radar;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getRockets() {
        return rockets;
    }

    public void setRockets(int rockets) {
        this.rockets = rockets;
    }

    public int getSits() {
        return sits;
    }

    public void setSits(int sits) {
        this.sits = sits;
    }

    public boolean isRadar() {
        return radar;
    }

    public void setRadar(boolean radar) {
        this.radar = radar;
    }

    public boolean getRadar() {
        return radar;
    }

    public String toString() {
        return "\n\ttype: " + getType() +
                "\n\trockets: " + getRockets() +
                "\n\tsits: " + getSits() +
                "\n\tradar: " + getRadar();
    }
}
