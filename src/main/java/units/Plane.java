package units;

public class Plane {
    private String model;
    private String origin;
    private Chars chars;
    private Parameters parameters;
    private long price;

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public Chars getChars() {
        return chars;
    }

    public void setChars(Chars chars) {
        this.chars = chars;
    }

    public Parameters getParameters() {
        return parameters;
    }

    public void setParameters(Parameters parameters) {
        this.parameters = parameters;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public String toString() {
        StringBuilder result = new StringBuilder(getModel());
        result.append("\nOrigin:");
        result.append(getOrigin());
        result.append("\nChars:");
        result.append(getChars());
        result.append("\nParameters:");
        result.append(getParameters());
        result.append("\nPrice:");
        result.append(getPrice());
        result.append("\n");
        return result.toString();
    }
}
