package units;

import java.util.ArrayList;

public class Planes {
    private ArrayList<Plane> plane;

    public ArrayList<Plane> getPlane() {
        return plane;
    }

    public void setPlane(ArrayList<Plane> plane) {
        this.plane = plane;
    }
}
