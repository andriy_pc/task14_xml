package parsing;

import org.xml.sax.*;
import org.xml.sax.helpers.DefaultHandler;
import units.Chars;
import units.Parameters;
import units.Plane;

import java.util.ArrayList;


public class SaxPars extends DefaultHandler {
    private static XMLReader xmlReader;

    private static ArrayList<Plane> planes;
    private String model;
    private String origin;

    private Chars chars;
    private String type;
    private int rockets;
    private int sits;
    private boolean radar;

    private Parameters parameters;
    private float length;
    private float width;
    private float height;

    private long price;

    private static String element;
    @Override
    public void startDocument() throws SAXException {
        planes = new ArrayList<>();
    }

    @Override
    public void endDocument() {
        System.out.println(planes);
    }

    @Override
    public void startElement(String namespaceURI, String localName,
                             String qName, Attributes atts) {
        element = qName;
    }

    @Override
    public void endElement(String namespaceURI,
                           String localName, String qName)
            throws SAXException {
        element = "";
    }

    @Override
    public void characters(char[] ch, int start, int length) {
        if(element.equalsIgnoreCase("Model")) {
            model = new String(ch, start, length);
        } else if(element.equalsIgnoreCase("Origin")) {
            origin = new String(ch, start, length);
        } else if (element.equalsIgnoreCase("Chars")) {
            chars = new Chars();
        } else if (element.equalsIgnoreCase("type")) {
            chars.setType(new String(ch, start, length));
        } else if (element.equalsIgnoreCase("rockets")) {
            chars.setRockets(Integer.parseInt(new String(ch, start, length)));
        } else if (element.equalsIgnoreCase("sits")) {
            chars.setSits(Integer.parseInt(new String(ch, start, length)));
        } else if (element.equalsIgnoreCase("radar")) {
            chars.setRadar(Boolean.parseBoolean(new String(ch, start, length)));
        } else if (element.equalsIgnoreCase("Parameters")) {
            parameters = new Parameters();
        } else if (element.equalsIgnoreCase("length")) {
            parameters.setLength(Float.parseFloat(new String(ch, start, length)));
        } else if (element.equalsIgnoreCase("width")) {
            parameters.setWidth(Float.parseFloat(new String(ch, start, length)));
        } else if(element.equalsIgnoreCase("height")) {
            parameters.setHeight(Float.parseFloat(new String(ch, start, length)));
        } else if(element.equalsIgnoreCase("Price")) {
            price = Long.parseLong(new String(ch, start, length));
            buildPlane();
        }
    }

    public void buildPlane() {
        Plane p = new Plane();
        p.setChars(chars);
        p.setModel(model);
        p.setOrigin(origin);
        p.setParameters(parameters);
        p.setPrice(price);
        planes.add(p);
    }

   /* public void setChars(Chars c) {
        chars = c;
    }

    public void setParameters(Parameters p) {
        parameters = p;
    }*/
}

class CharsHandler extends DefaultHandler {
    private ContentHandler topLevel;
    private static String element;
    private Chars result;

    CharsHandler(ContentHandler tl) {
        topLevel = tl;
    }
    @Override
    public void startDocument() throws SAXException {
        result = new Chars();
    }

    @Override
    public void startElement(String namespaceURI, String localName,
                             String qName, Attributes atts) {
        element = qName;
    }

    @Override
    public void endElement(String namespaceURI,
                           String localName, String qName)
            throws SAXException {
    }

    @Override
    public void characters(char[] ch, int start, int length) {

    }

}
