package parsing;

import org.xml.sax.SAXException;


import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;

public class Parse {
    public static void main(String[] args) throws SAXException,
            ParserConfigurationException, IOException {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser parser = factory.newSAXParser();
        SaxPars saxPars = new SaxPars();
        parser.parse(new File("planes\\Planes.xml"), saxPars);

    }
}
